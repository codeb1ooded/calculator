package com.codeb1ooded.megha.scientificcalculator;

/**
 * Created by megha on 12/7/16.
 */
public class Constants {

    public static String digitToConvert = "DIGIT_CONVERT";
    public static String conversionNumberSystem = "CONVERT_NUMBER_SYSTEM";
    public static String convertBinary = "CONVERT_BINARY_NUMBER";
    public static String convertOctal = "CONVERT_OCTAL_NUMBER";
    public static String convertDecimal = "CONVERT_DECIMAL_NUMBER";
    public static String convertHexadecimal = "CONVERT_HEXADECIMAL_NUMBER";

    public static String nothing = "NOTHING";
}
